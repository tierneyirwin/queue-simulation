package servicequeueproject;
/**
 * The ServiceQueue class initializes the service queues used in the program. 
 * This handles the customers in these lines, records the wait times, service times,
 * and idle times of their respective cashiers and customers.
 * 
 * @author Tierney Irwin
 * 
 * @DueDate 27 April 2016
 */
import java.util.LinkedList;

public class ServiceQueue extends LinkedList<Customer>
{
	private int myNumberCustomersServedSoFar;
	private int myNumberCustomersInLine;
	private long myTotalWaitTime;
	private long myTotalServiceTime;
	private int myTotalIdleTime;
	/**
	 * Constructor to initialize the service queue.
	 * Setting all variables to zero.
	 */
	public ServiceQueue()
	{
		myNumberCustomersServedSoFar=0;
		myNumberCustomersInLine=0;
		myTotalWaitTime=0;
		myTotalServiceTime=0;
		myTotalIdleTime=0;
	}
	/**
	 * Method to add to the idle time of the service queue, with the passed in int.
	 * 
	 * @param idle time to add to the total idle time
	 */
	public void addToIdleTime(int idle)
	{
		myTotalIdleTime=myTotalIdleTime+idle;
	}
	/**
	 * Method to add to the wait time of the service queue, with the passed in int.
	 * 
	 * @param l time to add to the total wait time
	 */
	public void addToWaitTime(long l)
	{
		myTotalWaitTime=myTotalWaitTime+l;
	}
	/**
	 * Method to add to the service time of the service queue, with the passed in int.
	 * 
	 * @param l time to add to the total service time
	 */
	public void addToServiceTime(long l)
	{
		myTotalServiceTime=myTotalServiceTime+l;
	}
	/**
	 * Method inserts a passed in customer 
	 * and increases that myNumberCustomersInLine 
	 * variable by 1.
	 * 
	 * @param customer Customer to be inserted into queue
	 */
	public void insertCustomer(Customer customer)
	{
		this.add(customer);
		myNumberCustomersInLine++;
		//System.out.println("Insert");
	}
	/**
	 * Method to return the first customer in line to be served.
	 * 
	 * @return Customer to be served
	 */
	public Customer serveCustomer()
	{
		Customer served = get(0);
		this.removeFirst();
		myNumberCustomersServedSoFar++;
		myNumberCustomersInLine--;
		return served;
	}
	/**
	 * Method to create the average wait time between all customers in the queue.
	 * 
	 * @return averageWaitTime the average wait time of the queue
	 */
	public long averageWaitTime()
	{
		if(myNumberCustomersServedSoFar==0)
		{
			return 0;
		}
		long averageWaitTime=myTotalWaitTime/(myNumberCustomersServedSoFar);
		return averageWaitTime;
	}
	/**
	 * Method to create the average service time of the queue between the customers served so far.
	 * 
	 * @return averageServiceTime the average service time of the queue
	 */
	public long averageServiceTime()
	{
		if(myNumberCustomersServedSoFar==0)
		{
			return 0;
		}
		long averageServiceTime=myTotalServiceTime/myNumberCustomersServedSoFar;
		return averageServiceTime;
	}
	/**
	 * Method to create the average idle time for the cashier of the queue over all the customers served.
	 * 
	 * @return averageIdleTime the average idle time over the entire queue for the cashier
	 */
	public int averageIdleTime()
	{
		if(myNumberCustomersServedSoFar==0)
		{
			return 0;
		}
		int averageIdleTime=myTotalIdleTime/(myNumberCustomersServedSoFar);
		return averageIdleTime;
	}
	/**
	 * Method to return the number of customers served in the queue so far.
	 * 
	 * @return myNumberCustomersServedSoFar the amount of customers already served
	 */
	public int getMyNumberCustomersServedSoFar() 
	{
		return myNumberCustomersServedSoFar;
	}
/**
 * Method to return the number of customers in the line.
 * 
 * @return myNumberCustomersInLine the number of customers in the queue
 */
	public int getMyNumberCustomersInLine() 
	{
		myNumberCustomersInLine=this.size();
		return myNumberCustomersInLine;
	}
/**
 * Method to return the total wait time of the queue.
 * 
 * @return myTotalWaitTime
 */
	public long getMyTotalWaitTime() 
	{
		return myTotalWaitTime;
	}
	/**
	 * Method to return the total service time of the queue.
	 * 
	 * @return myTotalServiceTime
	 */
	public long getMyTotalServiceTime() 
	{
		return myTotalServiceTime;
	}
	/**
	 * Method to return the total idle time of the queue.
	 * 
	 * @return myTotalIdleTime
	 */
	public int getMyTotalIdleTime() 
	{
		return myTotalIdleTime;
	}
}
