package servicequeueproject;
/**
 * The simulation model class grabs all values 
 * needed from the program to the view to portray 
 * stats and allow the controller to access components.
 * 
 * @author Tierney Irwin
 * @DueDate 27 April 2016
 *
 */
public class SimulationModel 
{
	private ServiceQueueManager myServiceQueueManager;
	private CustomerGenerator myCustomerGenerator;
	private Cashier[] myCashiers;
	/**
	 * Constructor to set up the model.
	 * 
	 * @param numberOfServiceQueues 
	 * @param maxCustomers max customers generated
	 * @param maxCashierTime max service time
	 * @param maxCustomerWait max time between customer generations
	 */
	public SimulationModel(int numberOfServiceQueues,int maxCustomers, int maxCashierTime,int maxCustomerWait)
	{
		myServiceQueueManager = new ServiceQueueManager(numberOfServiceQueues);
		myCashiers = new UniformCashier[numberOfServiceQueues];
		myCustomerGenerator = new UniformCustomerGenerator(maxCustomers,maxCustomerWait,myServiceQueueManager);
		for(int i =0;i<numberOfServiceQueues;i++)
		{
			myCashiers[i]=new UniformCashier(maxCashierTime,myServiceQueueManager.getMyServiceQueues()[i]);
		}
	}
	/**
	 * Method returns the overall total served in queues.
	 * 
	 * @return total served in all queues
	 */
	public long getOverallTotalServed()
	{
		return myServiceQueueManager.totalServedSoFar();
	}
	/**
	 * Method returns overall service time of queues.
	 * 
	 * @return overall service time of all queues.
	 */
	public long getOverallServiceTime()
	{
		return myServiceQueueManager.totalServiceTime();
	}
	/**
	 * Method returns overall idle time of queues.
	 * 
	 * @return overall idle time of all queues.
	 */
	public long getOverallIdleTime()
	{
		return myServiceQueueManager.getMyTotalIdleTime();
	}
	/**
	 * Method returns overall wait time of queues.
	 * 
	 * @return overall wait time of all queues.
	 */
	public long getOverallWaitTime()
	{
		return myServiceQueueManager.totalWaitTime();
	}
	/**
	 * Method returns overall average served of queues.
	 * 
	 * @return overall average served of queues.
	 */
	public int getOverallAverageServed()
	{
		return myServiceQueueManager.averageServed();
	}
	/**
	 * Method returns the shortest queue of the service queues.
	 * 
	 * @return shortest queue.
	 */
	public ServiceQueue shortestQueue()
	{
		return myServiceQueueManager.determineShortestQueue();
	}
	/**
	 * Method returns overall average wait time of queues.
	 * 
	 * @return overall average wait time of all queues.
	 */
	public float getOverallAverageWait()
	{
		return myServiceQueueManager.averageWaitTime();
	}
	/**
	 * Method returns overall average service time of queues.
	 * 
	 * @return overall average service time of all queues.
	 */
	public float getOverallAverageService()
	{
		return myServiceQueueManager.averageServiceTime();
	}
	/**
	 * Method returns overall average idle time of queues.
	 * 
	 * @return overall average idle time of all queues.
	 */
	public float getOverallAverageIdle()
	{
		return myServiceQueueManager.averageIdleTime();
	}
	/**
	 * Method returns the array of service queues
	 * 
	 * @return ServiceQueues[]
	 */
	public ServiceQueue[] getServiceQueues()
	{
		return myServiceQueueManager.getMyServiceQueues();
	}
	/**
	 * Method returns the start time of the program.
	 * 
	 * @return time that program was created in milliseconds
	 */
	public long getStartTime()
	{
		return myServiceQueueManager.getMyStartTime();
	}
	/**
	 * Method returns the current time of the program.
	 * 
	 * @return current time of program in milliseconds
	 */
	public long getPresentTime()
	{
		return myServiceQueueManager.getMyPresentTime();
	}
	/**
	 * Method returns customers in line of index passed in.
	 * 
	 * @return int customers in the service queue's line
	 */
	public int getCustomersInLine(int queue)
	{
		return myServiceQueueManager.getMyServiceQueues()[queue].getMyNumberCustomersInLine();
	}
	/**
	 * Method returns the customer generator.
	 * 
	 * @return myCustomerGenerator
	 */
	public CustomerGenerator Generator()
	{
		return myCustomerGenerator;
	}
	/**
	 * Method returns the cashier array.
	 * 
	 * @return myCashiers[]
	 */
	public Cashier[] Cashiers()
	{
		return myCashiers;
	}
}
