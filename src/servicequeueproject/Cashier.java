package servicequeueproject;
/**
 * The Cashier abstract class creates the instance of a cashier 
 * with the given fields and ties it to a certain service queue.
 * The cashier serves the customer class, records data for the service queue.
 * 
 * @author Tierney Irwin
 * @DueDate 27 April 2016
 */

public abstract class Cashier implements Runnable 
{
	private int myMaxTimeOfService;
	private ServiceQueue myServiceQueue;
	private Thread myThread;
	private boolean mySuspended;
	/**
	 * Constructor to initialize the cashier object.
	 * 
	 * @param maxTimeService int used to restrict how long a service time can be
	 * @param serviceQueue ServiceQueue that the cashier works for
	 */
	public Cashier(int maxTimeService,ServiceQueue serviceQueue)
	{
		myMaxTimeOfService=maxTimeService;
		myServiceQueue=serviceQueue;
		myThread = new Thread(this);
		mySuspended=false;
	}
	/**
	 * Method used to serve the customer generated. 
	 * This method records the customer's service and wait times.
	 * 
	 * @return long customer service time
	 */
	public long serveCustomer()
	{
		Customer customer = myServiceQueue.serveCustomer();
		customer.setMyServiceTime(generateServiceTime());
		long after = System.currentTimeMillis();
		long before = customer.getMyEntryTime();
		customer.setMyWaitTime(after-before);
		myServiceQueue.addToServiceTime(customer.getMyServiceTime());
		myServiceQueue.addToWaitTime(customer.getMyWaitTime());
		return customer.getMyServiceTime();
	}
	/**
	 * Method used to start the cashier thread that 
	 * continuously serves the customers in the service queue.
	 */
	public void start()
	{
		 try
	        {
	            myThread.start();
	        }
	        catch(IllegalThreadStateException e)
	        {
	            System.out.println("Thread already started");
	        }
	}
	/**
	 * Method returns the maximum 
	 * service time this cashier 
	 * can generate for a single customer.
	 * 
	 * @return myMaxTimeOfService int value 
	 * of the maximum time a cashier can serve a customer
	 */
	public int getMyMaxTimeOfService() 
	{
		return myMaxTimeOfService;
	}
	/**
	 * Method that runs the cashier thread. 
	 * This method adds idle time when there 
	 * are no customers or serves a customer 
	 * when they are there.
	 */
	@Override
	public void run() 
	{
		try
    	{
    		synchronized(this)
    		{
    			while(true)
    			{
    				this.waitWhileSuspended();
    				if(myServiceQueue.getMyNumberCustomersInLine()==0)
    				{
    					myServiceQueue.addToIdleTime(5);
    					Thread.sleep(5);
    				}
    				else
    				{
    	    			long sleep = this.serveCustomer();
    	    			Thread.sleep(sleep);
    				}
    			}
    		}
    	}
    	catch (InterruptedException e)
    	{
    		System.out.println("Thread suspended.");
    	}
	}
	/**
	 * Abstract method used to generate the service time of a customer.
	 * 
	 * @return int value of service time for a single customer
	 */
	public abstract int generateServiceTime();
	/**
	 * Method to pause the thread if mySuspended is true.
	 * 
	 * @throws InterruptedException
	 */
    private void waitWhileSuspended() throws InterruptedException
    {
    	while (mySuspended)
    	{
    		this.wait();
    	}
    }
    /**
     * Method used to suspend the thread, setting mySuspended to be true.
     */
    public void suspend()
    {
    	mySuspended = true;
    }
    /**
     * Method used to resume a thread from suspension. 
     * Also notifies the thread that mySuspended is set to false.
     */
    public synchronized void resume()
    {
    	mySuspended = false;
    	this.notify();
    }
}
