package servicequeueproject;
/**
 * The Uniform customer generator class adds the generate time between customers that the abstract class cashier did not have. 
 * This class uses a uniform distribution to generate the time between generating new customers,
 * based on the max allowed time.
 * 
 * @author Tierney Irwin
 * @DueDate 27 April 2016
 */
import java.util.Random;

public class UniformCustomerGenerator extends CustomerGenerator
{
	private Random myRandom = new Random();
	private int myMaxTimeBetweenCustomers;
	/**
	 * Basic constructor to send info to customer generator class.
	 * 
	 * @param maxCustomersGenerated max allowed customers to generate
	 * @param maxTimeBetweenCustomers max allowed time between generating customers
	 * @param serviceQueueManager manager to send info to
	 */
	public UniformCustomerGenerator(int maxCustomersGenerated, int maxTimeBetweenCustomers, ServiceQueueManager serviceQueueManager)
	{
		super(maxCustomersGenerated, maxTimeBetweenCustomers, serviceQueueManager);
		myMaxTimeBetweenCustomers=maxTimeBetweenCustomers;
	}
	/**
	 * Method to generate time between generating customers.
	 */
	public int generateTimeBetweenCustomers()
	{
		int timeBetweenCustomers = myRandom.nextInt(myMaxTimeBetweenCustomers);
		return timeBetweenCustomers;
	}


}
