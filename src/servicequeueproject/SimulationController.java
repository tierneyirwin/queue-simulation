package servicequeueproject;
/**
 * The Simulation Controller class runs the bridge 
 * between the view and the model in order to run the program.
 * The controller uses a thread to update the view with the code running in the model.
 * 
 * @author Tierney Irwin
 * @DueDate 27 April 2016
 */
import servicequeueproject.SimulationModel;
import servicequeueproject.SimulationController;
import servicequeueproject.SimulationView;

public class SimulationController implements Runnable
{
	//Data Members
	private SimulationModel myModel;
	private SimulationView myView;
	private boolean mySuspended;
	private Thread myThread;
	private boolean mySimulationRunning;
	
	/**
	 * Basic Constructor
	 */
	public SimulationController()
	{
		myView = new SimulationView(this);
        myThread = new Thread(this);
		mySuspended = false;
		mySimulationRunning = false;
	}
			
	/**
	 * Displays the customer images in the appropriate lines.
	 * @param index The queue index for the customers to be printed in.
	 */
	private void displayCustomers(int queue)
	{
		int numInQueue = myModel.getCustomersInLine(queue);
		myView.setCustomersInLine(queue, numInQueue);
	}
			
	/**
	 * Runs the thread that updates the view and starts the generator and cashiers. Also initializes the model.
	 */
    public void run()
    {
		int numServiceQueues = myView.getNumberServiceQueues();
		int maxCustomers = myView.getMaxCustomers();
		int maxCashierTime = myView.getMaxCashierTime();
		int maxCustomerWait = myView.getMaxBetweenTime();
		myModel = new SimulationModel(numServiceQueues, maxCustomers, maxCashierTime, maxCustomerWait);
    	try
    	{
    		synchronized(this)
    		{
    			myModel.Generator().start();
    			for(int i=0;i<myModel.Cashiers().length;i++)
    			{
    				myModel.Cashiers()[i].start();
    			}
    			this.updateView();
    		}
    	}
    	catch (InterruptedException e)
    	{
    		System.out.println("Thread suspended.");
    	}
    }
	
	/**
	 * Updates the view.
	 * @throws InterruptedException
	 */
	private void updateView() throws InterruptedException
	{
		while(true)
		{
			this.waitWhileSuspended();
			try 
			{
				if(myModel.getOverallTotalServed()==myView.getMaxCustomers())
				{
					myModel.Generator().suspend();
					for(int i=0;i<myModel.Cashiers().length;i++)
					{
						myModel.Cashiers()[i].suspend();
					}
					this.suspend();
				}
				Thread.sleep(100);
				// This updates the stats in the view.
				String message;
				String overall;
				String overflow;
				for(int x = 0; x < myModel.getServiceQueues().length; x++)
				{
					this.displayCustomers(x);
					this.getTotalServed();
					message = "Cashier "+x+"\nTotalServed: "+myModel.getServiceQueues()[x].getMyNumberCustomersServedSoFar()
							+"\nAvg. Service Time: "+myModel.getServiceQueues()[x].averageServiceTime()
							+"\nAvg. Idle Time: "+myModel.getServiceQueues()[x].averageIdleTime()
							+"\nAvg. Wait Time: "+myModel.getServiceQueues()[x].averageWaitTime();
					myView.displayStats(x,message);
					overall = "Total Served: "+"\n  "+myModel.getOverallTotalServed()+"\nTotal Service Time: "+"\n  "+myModel.getOverallServiceTime()
						+"\nTotal Idle Time: "+"\n  "+myModel.getOverallIdleTime()+"\nTotal Wait Time: "+"\n  "+myModel.getOverallWaitTime()
						+"\nAvg. served: "+"\n  "+myModel.getOverallAverageServed()+"\nAvg. Service Time: "+"\n  "+myModel.getOverallAverageServed()
						+"\nAvg. Idle Time: "+"\n  "+myModel.getOverallAverageIdle()+"\nAvg. Wait Time: "+"\n  "+myModel.getOverallAverageWait();
					myView.displayOverall(overall);
					if((myModel.getCustomersInLine(x)-6)<0)
					{
						overflow = "+"+(0);
						myView.displayOverflow(x, overflow);
					}
					else
					{
						overflow = "+"+(myModel.getCustomersInLine(x)-6);
						myView.displayOverflow(x, overflow);
					}
				}
			} 
			catch (InterruptedException e) 
			{
					e.printStackTrace();
			}
		}
	}
	/**
	 * Method to pause the thread if mySuspended is true.
	 * 
	 * @throws InterruptedException
	 */
    private void waitWhileSuspended() throws InterruptedException
    {
    	while (mySuspended)
    	{
    		this.wait();
    	}
    }
    /**
     * Method used to suspend the thread, setting mySuspended to be true.
     */
    public void suspend()
    {
    	mySuspended = true;
    }
	/**
	 * Method used to start the controller thread.
	 * 
	 */
    public void start()
    {
        try
        {
            myThread.start();
        }
        catch(IllegalThreadStateException e)
        {
            System.out.println("Thread already started");
        }
    }
    /**
     * Method used to resume a thread from suspension. 
     * Also notifies the thread that mySuspended is set to false.
     */
    public synchronized void resume()
    {
    	mySuspended = false;
    	this.notify();
    }
    /**
     * Method that dictates the functionality of the start button. 
     * Whether it pauses the threads, resumes them, or starts the entire program.
     */
    public void startPause()
    {
    	if(mySimulationRunning)
    	{
    		myView.changeStartPause();
    		if(mySuspended)
    		{
				myModel.Generator().resume();
				for(int i=0;i<myModel.Cashiers().length;i++)
				{
					myModel.Cashiers()[i].resume();
				}
    			this.resume();
    		}
    		else
    		{
				myModel.Generator().suspend();
				for(int i=0;i<myModel.Cashiers().length;i++)
				{
					myModel.Cashiers()[i].suspend();
				}
    			this.suspend();
    		}
    	}
    	else
    	{
    		mySimulationRunning=true;
    		myView.changeStartPause();
    		this.start();
    	}
    }
    /**
     * Method to calculate the total served and set the label for it.
     */
    public void getTotalServed()
    {
    	for(int i=0;i<myModel.getServiceQueues().length;i++)
    	{
    		String num = Integer.toString(myModel.getServiceQueues()[i].getMyNumberCustomersServedSoFar());
    		myView.setLabel(num, i);
    	}
    }
    /**
     * Main method that starts the controller.
     */
	public static void main(String[] args)
	{
		new SimulationController();
	}
}