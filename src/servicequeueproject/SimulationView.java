package servicequeueproject;
/**
 * The Simulation view class generates the GUI that 
 * displays the service queues and the stats for 
 * each as well as the overall stats.
 * The user sets the variables needed for the model to run.
 * 
 * @author Tierney Irwin
 * @DueDate 27 April 2016
 */
import java.awt.*;
import java.lang.reflect.Method;
import javax.swing.*;

@SuppressWarnings("serial")
public class SimulationView extends JFrame
{
	//Constants
	private final int TELLER_WIDTH = 75;
	private final int TELLER_HEIGHT = 85;
	private final String TELLER_IMG = "images/cashier.jpg";
	private final String FACE_IMG = "images/face.jpg";
	private final int COUNTER_BOX_WIDTH = 50;
	private final int COUNTER_BOX_HEIGHT = 20;
	private final int CUSTOMER_WIDTH = 50;
	private final int CUSTOMER_HEIGHT = 50;
	private final int ROW_1 = 400;
	private final int ROW_2 = 460; 
	private final int MAX_PEOPLE_IN_LINE = 6;
	private final int MAX_NUM_OF_TELLERS = 5;

	//Data Members
	private Image myScaledImage;
	private SimulationController myController;
	private Container myContentPane;
	private JLabel [] myTotalServed;
	private ButtonListener myStartPauseListener;
	private JLabel [][] myCustomer;
	private JButton myStartPauseButton;
	private JLabel [] myTeller;
	private JPanel mySimPanel;
	private JPanel myEastPanel;
	private JPanel myWestPanel;
	private JTextField myGenerateTime;
	private JTextField myMaxNumberOfCustomer;
	private JComboBox<Integer> myNumberOfTellers;
	private JTextField myMaxServiceTime;
	private JTextArea myOverallStats;
	private JTextArea[] myCashierStats;
	private JPanel myNorthPanel;
	private JLabel[] myOverflow;
	
	/**
	 * Constructor that creates the view.
	 * 
	 * @param controller the SimulationController that gives function to the buttons.
	 */
	public SimulationView(SimulationController controller)
	{
		Image face = Toolkit.getDefaultToolkit().getImage(FACE_IMG);
		myScaledImage = face.getScaledInstance(CUSTOMER_WIDTH,CUSTOMER_HEIGHT,Image.SCALE_SMOOTH);
		
		myController = controller;
		
		//Start/Pause Button
		myStartPauseButton = new JButton("Start");
		
		this.associateListeners(myController);

		//Frame info
		this.setSize(800,650);
		this.setLocation(100, 100);
		this.setTitle("ServiceQueue");
		this.setResizable(false);
		
		myContentPane = getContentPane();
		myContentPane.setLayout(new BorderLayout());
		
		//Sim Panel
		mySimPanel = new JPanel();
		mySimPanel.setBorder(BorderFactory.createLoweredBevelBorder());
		mySimPanel.setLayout(null);
		
		//Customer Served Counter
		myTotalServed = new JLabel[MAX_NUM_OF_TELLERS];
		
		for(int i = 0; i < myTotalServed.length; i++)
		{
			myTotalServed[i] = new JLabel("0");
			myTotalServed[i].setSize(COUNTER_BOX_WIDTH, COUNTER_BOX_HEIGHT);
			myTotalServed[i].setLocation(40+(CUSTOMER_WIDTH*2*i), ROW_2);
			myTotalServed[i].setBorder
					(BorderFactory.createLineBorder(Color.BLACK));
			mySimPanel.add(myTotalServed[i]);
		}

		//Teller locations
		myTeller = new JLabel[MAX_NUM_OF_TELLERS];
		
		for(int i = 0; i < MAX_NUM_OF_TELLERS; i++)
		{
			myTeller[i] = new JLabel(new ImageIcon(TELLER_IMG));
			myTeller[i].setSize(TELLER_WIDTH, TELLER_HEIGHT);
			myTeller[i].setLocation(25+(CUSTOMER_WIDTH*2*i), ROW_1);
			myTeller[i].setVisible(true);
			mySimPanel.add(myTeller[i]);
		}
	
		//Customer Lines
		myCustomer = new JLabel[MAX_NUM_OF_TELLERS][MAX_PEOPLE_IN_LINE];
		for(int i = 0; i < MAX_NUM_OF_TELLERS; i++)
		{
			for(int j = 0; j < MAX_PEOPLE_IN_LINE; j++)
			{
				myCustomer[i][j] = new JLabel();
				myCustomer[i][j].setSize(CUSTOMER_WIDTH, CUSTOMER_HEIGHT);
				myCustomer[i][j].setLocation(40 + (CUSTOMER_WIDTH*2*i), 325 - (50*j));
				myCustomer[i][j].setVisible(true);
				mySimPanel.add(myCustomer[i][j]);
			}
		}
		
		//Background
		JLabel bg;
		bg = new JLabel(new ImageIcon("images/background.jpg"));
		bg.setSize(600, 485);
		bg.setLocation(0, 0);
		mySimPanel.add(bg);
		myContentPane.add(mySimPanel, BorderLayout.CENTER);
		
		myNorthPanel = new JPanel();
		myContentPane.add(myNorthPanel, BorderLayout.NORTH);
		myOverflow = new JLabel[5];
		myNorthPanel.setBorder(BorderFactory.createLoweredBevelBorder());
		
		for(int i = 0; i < myOverflow.length; i++)
		{
			myOverflow[i] = new JLabel("0");
			myOverflow[i].setPreferredSize(new Dimension(COUNTER_BOX_WIDTH, COUNTER_BOX_HEIGHT));
			myOverflow[i].setLocation(40+(CUSTOMER_WIDTH*3*i), ROW_2);
			myOverflow[i].setBorder
					(BorderFactory.createLineBorder(Color.BLACK));
			myNorthPanel.add(myOverflow[i]);
		}
		
		//overall stats panel
		myEastPanel = new JPanel();
		myEastPanel.setPreferredSize(new Dimension(130,60));
		JPanel overall = new JPanel();
		myOverallStats = new JTextArea(10,10);
		myOverallStats.setEditable(false);
		myOverallStats.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		myEastPanel.add(overall);
		overall.add(myOverallStats);
		
		//generate time label
		myGenerateTime = new JTextField(5);
		JPanel test2 = new JPanel();
		test2.setLayout(new FlowLayout());
		myEastPanel.add(test2);
		test2.add(new JLabel("Generate Time: "));
		test2.add(myGenerateTime);
		
		//num customers label
		JPanel numCust = new JPanel();
		myMaxNumberOfCustomer = new JTextField(5);
		numCust.setLayout(new FlowLayout());
		myEastPanel.add(numCust);
		numCust.add(new JLabel("# of Customers: "));
		numCust.add(myMaxNumberOfCustomer);
		
		//Tellers label
		JPanel tellers = new JPanel();
		String[] num = {"1","2","3","4","5"};
		myNumberOfTellers = new JComboBox(num);
		tellers.setLayout(new FlowLayout());
		myEastPanel.add(tellers);
		tellers.add(new JLabel("# of Cashiers: "));
		tellers.add(myNumberOfTellers);
		
		//Service Time
		JPanel service = new JPanel();
		myMaxServiceTime = new JTextField(5);
		service.setLayout(new FlowLayout());
		myEastPanel.add(service);
		service.add(new JLabel("Max Service Time: "));
		service.add(myMaxServiceTime);
		
		myContentPane.add(myStartPauseButton,BorderLayout.SOUTH);
		myContentPane.add(myEastPanel,BorderLayout.EAST);
		myEastPanel.setLayout(new BoxLayout(myEastPanel,BoxLayout.Y_AXIS));
		myEastPanel.setSize(20, 20);
		
		
		myWestPanel= new JPanel();
		myCashierStats = new JTextArea[MAX_NUM_OF_TELLERS];
		myContentPane.add(myWestPanel, BorderLayout.WEST);
		myWestPanel.setLayout(new BoxLayout(myWestPanel, BoxLayout.Y_AXIS));
		
		for(int i = 0; i < MAX_NUM_OF_TELLERS; i++)
		{
			myCashierStats[i] = new JTextArea(12,12);
			myCashierStats[i].setBorder(BorderFactory.createLineBorder(Color.BLACK));
			myCashierStats[i].setVisible(true);
			myWestPanel.add(myCashierStats[i]);
			myCashierStats[i].setEditable(false);
		}

		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);	
	}
	
	//////////////////////////////////////////
	//            Methods                   //
	//////////////////////////////////////////

	/**
	 * Method changes the look of the 
	 * start/pause button based on when 
	 * it is pressed.
	 */
	public void changeStartPause() 
	{
		if(myStartPauseButton.getText().equals("Start"))
		{
			myStartPauseButton.setText("Pause");
		}
		else
		{
			myStartPauseButton.setText("Start");
		}
	}
	/**
	 * Method sets the images of the amount of customers in line.
	 * 
	 * @param queue which queue the customer enters
	 * @param numInLine the number in line currently
	 */
	 public void setCustomersInLine(int queue, int numInLine)
	 {
		myTeller[queue].setIcon(new ImageIcon(TELLER_IMG));
		
		for(int i = 0; i < MAX_PEOPLE_IN_LINE; i++)
		{
			myCustomer[queue][i].setVisible(false);
		}
		try
		{
			for(int i = 0; i < numInLine && i < MAX_PEOPLE_IN_LINE; i++)
			{
				myCustomer[queue][i].setVisible(true);
			    myCustomer[queue][i].setIcon(new ImageIcon(myScaledImage));
			} 
		}
		catch (NullPointerException e)
		{
			
		}
	 }
	 
	/**
	 * Associates the button with the appropriate method
	 * @param controller The controller in which the method is included.
	 */
	private void associateListeners(SimulationController controller)
	{
		Class<? extends SimulationController> controllerClass;
		Method startPauseMethod;
		
		controllerClass = myController.getClass();
				
		startPauseMethod = null;
		
		try 
		{
			startPauseMethod = 
				controllerClass.getMethod("startPause", (Class<?>[])null);
		} 
		catch (SecurityException e) 
		{	
			String error;

			error = e.toString();
			System.out.println(error);
		} 
		catch (NoSuchMethodException e) 
		{
			String error;

	        error = e.toString();
	        System.out.println(error);
		}

		myStartPauseListener = 
			new ButtonListener(myController, startPauseMethod, null);
				
		myStartPauseButton.addMouseListener(myStartPauseListener);
	}
	/**
	 * Method returns the string put in the max customers jtextfield from user.
	 * 
	 * @return integer from the jtextfield
	 */
	public int getMaxCustomers()
	{
		String max = myMaxNumberOfCustomer.getText();
		int numCustomer = Integer.parseInt(max,10);
		return numCustomer;
	}
	/**
	 * Method returns the string put in the max service time jtextfield from user.
	 * 
	 * @return integer from the jtextfield
	 */
	public int getMaxCashierTime()
	{
		String max = myMaxServiceTime.getText();
		int service = Integer.parseInt(max,10);
		return service;
	}	
	/**
	 * Method returns the string put in the max time between generating customers jtextfield from user.
	 * 
	 * @return integer from the jtextfield
	 */
	public int getMaxBetweenTime()
	{
		String time = myGenerateTime.getText();
		int between = Integer.parseInt(time,10);
		return between;
	}
	/**
	 * Method returns the string put in the number of tellers combobox from user.
	 * 
	 * @return integer from the jtextfield
	 */
	public int getNumberServiceQueues()
	{
		Object max = myNumberOfTellers.getSelectedItem();
		int tellers = Integer.parseInt(max.toString(),10);
		return tellers;
	}
	/**
	 * Method sets the labels to new text for the totalServed array.
	 * 
	 * @param num new text for label
	 * @param queue which index of the array
	 */
	public void setLabel(String num, int queue)
	{
		myTotalServed[queue].setText(num);
	}
	/**
	 * Method sets the stats for the cashier passed in with the new stats.
	 * 
	 * @param cashier which cashier is updated
	 * @param message the new stats message
	 */
	public void displayStats(int cashier,String message)
	{
		myCashierStats[cashier].setText(message);
	}
	/**
	 * Method sets the stats for the overall program with the new stats.
	 * 
	 * @param message the new stats message
	 */
	public void displayOverall(String message)
	{
		myOverallStats.setText(message);
	}
	/**
	 * Method sets the overflow boxs when the line gets longer than the max number allowed in the view.
	 * 
	 * @param index which line it is
	 * @param message how many customers are in line passed the ones shown
	 */
	public void displayOverflow(int index, String message)
	{
		myOverflow[index].setText(message);
	}
}