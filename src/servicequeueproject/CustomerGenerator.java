package servicequeueproject;
/**
 * The CustomerGenerator abstract class generates the 
 * amount of customers for the model based on 
 * the parameters passed in.
 * 
 * @author Tierney Irwin
 * @DueDate 27 April 2016
 */
public abstract class CustomerGenerator implements Runnable
{
	private long myMaxTimeBetweenCustomers;
	private int myMaxCustomersGenerated;
	private ServiceQueueManager myServiceQueueManager;
	private Thread myThread;
	private boolean mySuspended;
	/**
	 * Constructor that initializes the customer generator with the parameters provided.
	 * 
	 * @param maxCustomersGenerated int maximum customers that should be generated
	 * @param maxTimeBetweenCustomers long maximum time between generating customers
	 * @param serviceQueueManager ServiceQueueManager for all the service queues
	 */
	public CustomerGenerator(int maxCustomersGenerated, long maxTimeBetweenCustomers,ServiceQueueManager serviceQueueManager)
	{
		myMaxTimeBetweenCustomers = maxTimeBetweenCustomers;
		myMaxCustomersGenerated = maxCustomersGenerated;
		myServiceQueueManager = serviceQueueManager;
		myThread = new Thread(this);
		mySuspended=false;
	}
	/**
	 * Abstract method that generates the time between customers generated.
	 * 
	 * @return int time between customers generated.
	 */
	public abstract int generateTimeBetweenCustomers();

	/**
	 * Method returns a new customer with the present time as its entry time.
	 * 
	 * @param time time the customer is created.
	 * 
	 * @return new customer for the service queue
	 */
	public Customer generateCustomer(long time)
	{
		return new Customer(myServiceQueueManager.getMyPresentTime());
	}
	/**
	 * Method used to start the customer generator thread.
	 * 
	 */
	public void start()
	{
		 try
	        {
	            myThread.start();
	        }
	        catch(IllegalThreadStateException e)
	        {
	            System.out.println("Thread already started");
	        }
	}
	/**
	 * Method that dictates what runs when the thread is started. 
	 * The customer generator generates a customer and sleeps 
	 * for that generated time. That customer is inserted into the queue with the shortest line.
	 */
	@Override
	public void run() 
	{
		try
    	{
    		synchronized(this)
    		{
    			for(int i=0;i<myMaxCustomersGenerated;i++)
    			{
    				this.waitWhileSuspended();
    				int time=generateTimeBetweenCustomers();
    				Customer customer = this.generateCustomer(time);
    				ServiceQueue queue = myServiceQueueManager.determineShortestQueue();
    				queue.insertCustomer(customer);
    				Thread.sleep(time);
    			}
    		}
    	}
    	catch (InterruptedException e)
    	{
    		System.out.println("Thread suspended.");
    	}
		
	}
/**
 * Method returns the maximum time between customers to be generated.
 * 
 * @return long value of maximum time between customers
 */
	public long getMyMaxTimeBetweenCustomers() 
	{
		return myMaxTimeBetweenCustomers;
	}
	/**
	 * Method returns the maximum customers generated.
	 * 
	 * @return myMaxCustomersGenerated int of the maximum number customers
	 */
	public int getMyMaxCustomersGenerated() 
	{
		return myMaxCustomersGenerated;
	}
	/**
	 * Method returns the service queue manager.
	 * 
	 * @return ServiceQueueManager
	 */
	public ServiceQueueManager getMyServiceQueueManager() 
	{
		return myServiceQueueManager;
	}
	/**
	 * Method to pause the thread if mySuspended is true.
	 * 
	 * @throws InterruptedException
	 */
    private void waitWhileSuspended() throws InterruptedException
    {
    	while (mySuspended)
    	{
    		this.wait();
    	}
    }
    /**
     * Method used to suspend the thread, setting mySuspended to be true.
     */
    public void suspend()
    {
    	mySuspended = true;
    }
    /**
     * Method used to resume a thread from suspension. 
     * Also notifies the thread that mySuspended is set to false.
     */
    public synchronized void resume()
    {
    	mySuspended = false;
    	this.notify();
    }

}
