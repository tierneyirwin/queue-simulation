package servicequeueproject;
/**
 * The ServiceQueueManager class handles all the data 
 * over all the service queues and runs which customers go where.
 * 
 * @author Tierney Irwin
 * @DueDate 27 April 2016
 *
 */
public class ServiceQueueManager 
{
	public final int MAX_NUMBER_OF_QUEUES=5;
	private int myNumberOfServiceQueues;
	private ServiceQueue[] myServiceQueues;
	private long myTotalWaitTime;
	private long myTotalServiceTime;
	private int myTotalIdleTime;
	private float myAverageWaitTime;
	private float myAverageServiceTime;
	private int myAverageIdleTime;
	private long myPresentTime;
	private long myStartTime;
	/**
	 * Constructor to initialize the variables inside the service queue manager.
	 * 
	 * @param numberServiceQueues number of service queues to be generated.
	 */
	public ServiceQueueManager(int numberServiceQueues)
	{
		myTotalWaitTime=0;
		myTotalServiceTime=0;
		myTotalIdleTime=0;
		myAverageWaitTime=0;
		myAverageServiceTime=0;
		myAverageIdleTime=0;
		myStartTime=System.currentTimeMillis();
		myPresentTime=System.currentTimeMillis();
		setMyNumberOfServiceQueues(numberServiceQueues);
		myServiceQueues = new ServiceQueue[myNumberOfServiceQueues];
		for(int i=0;i<myNumberOfServiceQueues;i++)
		{
			myServiceQueues[i] = new ServiceQueue();
		}
	}
	/**
	 * Method to calculate the total served overall the queues currently.
	 * 
	 * @return served the total served from all queues
	 */
	public int totalServedSoFar()
	{
		int served=0;
		for(int i=0;i<myNumberOfServiceQueues;i++)
		{
			served=served+myServiceQueues[i].getMyNumberCustomersServedSoFar();
		}
		return served;
	}
	/**
	 * Method to return the average served over all the queues.
	 * 
	 * @return average served from all queues
	 */
	public int averageServed()
	{
		return totalServedSoFar()/myServiceQueues.length;
	}
	/**
	 * Method to calculate the total wait time over all queues.
	 * 
	 * @return myTotalWaitTime
	 */
	public long totalWaitTime()
	{
		for(int i=0;i<myNumberOfServiceQueues;i++)
		{
			myTotalWaitTime=myTotalWaitTime+myServiceQueues[i].getMyTotalWaitTime();
		}
		return myTotalWaitTime;
	}
	/**
	 * Method to calculate the total service time over all the queues.
	 * 
	 * @return myTotalServiceTime
	 */
	public long totalServiceTime()
	{
		for(int i=0;i<myNumberOfServiceQueues;i++)
		{
			myTotalServiceTime=myTotalServiceTime+myServiceQueues[i].getMyTotalServiceTime();
		}
		return myTotalServiceTime;
	}
	/**
	 * Method that returns the shortest queue out of all the service queues.
	 * 
	 * @return shortest the service queue with the smallest amount of customers in line.
	 */
	public ServiceQueue determineShortestQueue()
	{
		ServiceQueue shortest=myServiceQueues[0];
		for(int i=0;i<myServiceQueues.length;i++)
		{
			if(shortest.size()>myServiceQueues[i].size())
			{
				shortest=myServiceQueues[i];
			}
		}
		return shortest;
	}
	/**
	 * Method to return the average wait time out of all the queues.
	 * 
	 * @return the average wait time of all queues.
	 */
	public float averageWaitTime()
	{
		totalWaitTime();
		return myTotalWaitTime/myServiceQueues.length;
	}
	/**
	 * Method to return the average service time out of all the queues.
	 * 
	 * @return the average service time of all queues.
	 */
	public float averageServiceTime()
	{
		totalServiceTime();
		return myTotalServiceTime/myServiceQueues.length;
	}
	/**
	 * Method to return the average idle time out of all the queues.
	 * 
	 * @return the average idle time of all queues.
	 */
	public float averageIdleTime()
	{
		getMyTotalIdleTime();
		return myTotalIdleTime/myServiceQueues.length;
	}
/**
 * Method to return the maximum number of queues possible
 * 
 * @return MAX_NUMBER_OF_QUEUES
 */
	public int getMAX_NUMBER_OF_QUEUES() 
	{
		return MAX_NUMBER_OF_QUEUES;
	}
/**
 * Method to return the number of service queues currently.
 * 
 * @return myNumberOfServiceQueues
 */
	public int getMyNumberOfServiceQueues() 
	{
		return myNumberOfServiceQueues;
	}
	/**
	 * Method to set the number of service queues to the parameter passed in.
	 * 
	 * @param numberOfServiceQueues new number of service queues
	 */
	public void setMyNumberOfServiceQueues(int numberOfServiceQueues)
	{
		myNumberOfServiceQueues=numberOfServiceQueues;
	}
	/**
	 * Method to return the array of service queues n the program.
	 * 
	 * @return myServiceQueues array
	 */
	public ServiceQueue[] getMyServiceQueues() 
	{
		return myServiceQueues;
	}
	/**
	 * Method to return the total idle time of all the queues.
	 * 
	 * @return myTotalIdleTime
	 */
	public int getMyTotalIdleTime() 
	{
		for(int i=0;i<myNumberOfServiceQueues;i++)
		{
			myTotalIdleTime=myTotalIdleTime+myServiceQueues[i].getMyTotalIdleTime();
		}
		return myTotalIdleTime;
	}
	/**
	 * Method to return the current time of the program.
	 * 
	 * @return myPresentTime
	 */
	public long getMyPresentTime() 
	{
		myPresentTime = System.currentTimeMillis();
		return myPresentTime;
	}
	/**
	 * Method to return the time the program started.
	 * 
	 * @return myStartTime
	 */
	public long getMyStartTime() 
	{
		return myStartTime;
	}
	
	
}
