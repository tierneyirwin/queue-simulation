package servicequeueproject;
/**
 * The Customer class is a representation of the customers 
 * approaching the service queue, holding their wait, 
 * service, and entry times for use in the program.
 * 
 * @author Tierney Irwin
 * @DueDate 27 April 2016
 */
public class Customer 
{
	private long myServiceTime;
	private long myEntryTime;
	private long myWaitTime;
	/**
	 * Constructor that initializes the customer.
	 * 
	 * @param entryTime long time that the customer 
	 * 	is created in the program in milliseconds
	 */
	public Customer(long entryTime)
	{
		myWaitTime=0;
		myEntryTime=entryTime;
	}
	/**
	 * Method used to return the service time of a customer.
	 * 
	 * @return long value of the service time of a customer
	 */
	public long getMyServiceTime() 
	{
		return myServiceTime;
	}
	/**
	 * Method used to return the entry time of a customer.
	 * 
	 * @return long value of the entry time of a customer
	 */
	public long getMyEntryTime() 
	{
		return myEntryTime;
	}
	/**
	 * Method used to return the wait time of a customer.
	 * 
	 * @return long value of the wait time of a customer
	 */
	public long getMyWaitTime() 
	{
		return myWaitTime;
	}
	/**
	 * Method used to set the service time of a customer.
	 * 
	 * @param myServiceTime int value of new service time for customer
	 */
	public void setMyServiceTime(int myServiceTime) 
	{
		this.myServiceTime = myServiceTime;
	}
	/**
	 * Method used to set the wait time of a customer.
	 * 
	 * @param myServiceTime int value of new wait time for customer
	 */
	public void setMyWaitTime(long l) 
	{
		this.myWaitTime = l;
	}
	
}
