package servicequeueproject;
/**
 * The Uniform cashier class adds the generate service time that the abstract class cashier did not have. 
 * This class uses a uniform distribution to generate the service time of a customer,
 * based on the max allowed service time.
 * 
 * @author Tierney Irwin
 * @DueDate 27 April 2016
 */
import java.util.Random;

public class UniformCashier extends Cashier
{
	private Random myRandom = new Random();
	private int myMaxTimeOfService;
	/**
	 * Basic constructor to send info to cashier class.
	 * 
	 * @param maxTimeService max service time allowed
	 * @param serviceQueue which service queue this cashier is assigned to
	 */
	public UniformCashier(int maxTimeService, ServiceQueue serviceQueue)
	{
		super(maxTimeService,serviceQueue);
		myMaxTimeOfService=maxTimeService;
	}
	/**
	 * Method used to generate the service time using a random number generator between 0 and the max allowed.
	 * @return int amount of service time for customer
	 */
	public int generateServiceTime()
	{
		int serviceTime = myRandom.nextInt(myMaxTimeOfService);
		return serviceTime;
	}
}
