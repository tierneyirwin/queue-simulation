# README #

### What is this repository for? ###

* Service Queue simulation where user inputs values for amount of cashiers, speed of customer creation, etc to showcase how a cashier line will run with the given variables.

### How do I get set up? ###

* Input values into the sidebar to change the simulation. Press the run button to execute the program with the inputted variables.